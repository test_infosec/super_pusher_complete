import logging
import pprint

from rt import RtError
from setup import assemblyline_url, rtir_connection


def create_text(sid, text):
    url = '<a href="{}" target="_blank">Click here</a><br>'.format \
        ('{}/submission_detail.html?sid='.format(assemblyline_url, ) + sid)

    s = ""
    s += text
    s += "<br /><br />"
    s += url
    return s


def create_ticket(sid, subject, text):
    t_dict = dict()
    t_dict['Content-Type'] = 'text/html'
    t_dict['Subject'] = subject
    t_dict['Text'] = create_text(sid, text)
    try:
        rt = rtir_connection()
        rt.login()
        rt.create_ticket(**t_dict)
        rt.logout()
    except RtError as e:
        log = logging.getLogger('log')
        log.error('Failed to create RTir ticket: %s', e[0])


def ticket(sub):
    log = logging.getLogger('log')
    log.info('TS: Logging %s', sub['files'][0][0])

    create_ticket(sub['submission']['sid'], sub['submission']['description'], pprint.pformat(sub['submission']))
