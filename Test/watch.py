from assemblyline_client import Client
import os
from multiprocessing import Process
import time
import signal
import sys
from shutil import copyfile
import random
import pprint
import yaml


   


def callback(callback_data):
    print callback_data

def _main():
    
    with open("config",'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    print str(cfg)
    queue=cfg['Queue']
    users=cfg['Users']
       
    try:
        al_client = Client(cfg['Adresse'], apikey=(users[0][0], users[0][1]), verify=False)
    except Exception as e:
        print "fail"
        print e
    finally:
        print "ok"
        print queue
    try:
        al_client.socketio.listen_on_watch_queue(queue,result_callback=callback)
    except Exception as e:
        print e
    

if __name__ == '__main__':
    _main()
