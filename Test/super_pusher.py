from assemblyline_client import Client
import os
from multiprocessing import Process
import time
import signal
import sys
from shutil import copyfile
import random
import pprint
import yaml

# http://code.activestate.com/recipes/410695-exception-based-switch-case/
class case_selector(Exception):
   def __init__(self, value): # overridden to ensure we've got a value argument
      Exception.__init__(self, value)

def switch(variable):
   raise case_selector(variable)

def case(value):
   exclass, exobj, tb = sys.exc_info()
   if exclass is case_selector and exobj.args[0] == value: return exclass
   return None
   
   
def handler(signum, frame):
    al_client = Client("https://localhost", auth=('temp', '1a2b3c4dazertY'), verify=False)
    for submission in al_client.search.stream.submission("submission.submitter:temp"):
        print "Deleting: "+submission['submission.sid']
        print al_client.submission.delete(submission['submission.sid'])
    sys.exit()

def callback(callback_data):
    print callback_data

def _main():
    print os.getpid()
    with open("config",'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    print str(cfg)
    nq = cfg['Queue']
    users = cfg['Users']
    #pprint.pprint(cfg)
    

    
    try:
        al_client = Client(cfg['Adresse'], apikey=(users[0][0], users[0][1]), verify=False)
    except Exception as e:
        print "fail"
        print e
    #al_client.socketio.listen_on_watch_queue(queue,result_callback=callback)
    th=Process(target=f,args=())
    th.start()
    signal.signal(signal.SIGINT, handler)
    
    while True:
        for eml in os.listdir('/home/infosec/Desktop/tmp/'):
            print"\n\n -------------Main-Process"
            print eml
            resul= al_client.ingest('/home/infosec/Desktop/tmp/'+eml, params={'ignore_cache':True},nq=nq)
            #print type(resul)
            #print dir(resul)
            pprint.pprint(resul)
            #print resul['submission']['sid']
            os.remove('/home/infosec/Desktop/tmp/'+eml)
            """"
            wq=al_client.live.setup_watch_queue(resul['submission']['sid'])
            print wq
            while True:
                temp=[]
                tempbis=al_client.live.get_message_list(resul['submission']['sid'])
                #if not tempbis == temp:
                print tempbis
            """
            #al_client.socketio.listen_on_submissions_ingested(callback)
            #time.sleep(random.randint(1,80) /10.0)
        #time.sleep(1)  
        #for eml in os.listdir('/home/infosec/Desktop/eml/'):
        #    copyfile('/home/infosec/Desktop/eml/'+eml, '/home/infosec/Desktop/tmp/'+eml)
        time.sleep(1)

def get_source(name,valid_source):
    end=name.find(']')
    if end == -1:
        source="Default"
    else:
        source = name[1:end]
    if not source in valid_source:
        source="Default"
    return source

def f():
    with open("config",'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    client = Client(cfg['Adresse'], apikey=(cfg['Users'][0][0], cfg['Users'][0][1]), verify=False)
    valid_source= cfg['Source'].keys()
    queue=cfg['Queue']
    while True:
        try:
            ght=client.ingest.get_message_list(queue)
            if not len(ght)==0:
                print len(ght)
            for msg in ght:
                print"\n\n -------------Sub-Process"
                """
                #sub= client.submission.full(msg['alert']['sid'])
                print sub['files'][0][0]
                #print sub['submission']['max_score']
                source = get_source(sub['files'][0][0],valid_source)
                print source
                if source in valid_source:
                    for module in cfg['Source'][source]:
                        if cfg['Source'][source][module]:
                            try:
                                switch(module)
                            except case('ELK'):
                                print "ELK"
                            except case('TS'):
                                print "TS"
                """     
                #pprint.pprint(client.submission(msg['alert']['sid']))
        except Exception:
            pass
        time.sleep(1)

if __name__ == '__main__':
    _main()
